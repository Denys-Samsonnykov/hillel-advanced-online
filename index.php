<?php
interface MotherLogger
{
    public function format(Logger $string);


    public function deliver(Logger $format);


}

class Logger
{

    private $format;
    private $delivery;

    public function __construct($format, $delivery)
    {
        $this->format = $format;
        $this->delivery = $delivery;
    }

    public function log($string)
    {
        $this->deliver($this->format($string));
    }

    public function format($string)
    {
        switch ($this->format) {
            case 'raw' :
                {
                    return $string;
                }
                break;
            case 'with_date':
                {
                    return date('Y-m-d H:i:s') . $string;
                }
                break;
            case 'with_date_and_details':
                {
                    return date('Y-m-d H:i:s') . $string . ' - With some details';
                }
                break;
            default:
            {
                die('Error format');
            }
        }
    }

    public function deliver($format)
    {
        switch ($this->delivery) {
            case 'by_email' :
                {
                    echo "Вывод формата ({$format}) по имейл";
                }
                break;
            case 'by_sms':
                {
                    echo "Вывод формата ({$format}) в смс";
                }
                break;
            case 'to_console':
                {
                    echo "Вывод формата ({$format}) в консоль";
                }
                break;
            default:
            {
                die('Error deliver');
            }
        }
    }

}
class LoggerChanges extends Logger implements MotherLogger
{
    public function __construct($format, $delivery)
    {
        parent::__construct($format, $delivery);
    }

    public function format(Logger $string)
    {

    }

    public function deliver(Logger $format)
    {

    }

}

$logger = new Logger('raw', 'by_sms');
$logger->log('Emergency error! Please fix me!');




